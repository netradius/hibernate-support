/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This user type handles encrypting and decrypting strings.
 * 
 * @author Erik R. Jensen
 */
public abstract class EncryptedStringUserType extends EncryptedUserType implements ParameterizedType {

	/**
	 * The character set parameter name, "charset".
	 */
	public static final String PARAM_CHARSET = "charset";

	/**
	 * The default character set, UTF-8.
	 */
	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF8");

	/**
	 * The character set to use which is by default UTF-8.
	 */
	protected Charset charset = DEFAULT_CHARSET;

	/**
	 * {@inheritDoc}
	 */
	public void setParameterValues(final Properties properties) {
		if (properties != null) {
			final String charset = properties.getProperty(PARAM_CHARSET);
			if (charset != null) {
				this.charset = Charset.forName(charset);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Class returnedClass() {
		return String.class;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean equals(final Object o, final Object o1) throws HibernateException {
		return o == o1 || (o != null && o.equals(o1));
	}

	/**
	 * {@inheritDoc}
	 */
	public int hashCode(final Object o) throws HibernateException {
		return o.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	public Object deepCopy(final Object o) throws HibernateException {
		return o;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isMutable() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public Serializable disassemble(final Object o) throws HibernateException {
		return (Serializable)o;
	}

	/**
	 * {@inheritDoc}
	 */
	public Object assemble(final Serializable serializable, final Object o) throws HibernateException {
		return serializable;
	}

	/**
	 * {@inheritDoc}
	 */
	public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
		return original;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object nullSafeGet(
	    final ResultSet rs, final String[] names, final SharedSessionContractImplementor impl, final Object owner)
			throws HibernateException, SQLException {
		final Object o = super.nullSafeGet(rs, names, impl, owner);
		return o != null
				? new String((byte[])o, charset)
				: null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void nullSafeSet(
	    final PreparedStatement ps, final Object o, final int index, final SharedSessionContractImplementor impl)
			throws HibernateException, SQLException {
		super.nullSafeSet(ps, o == null ? null : ((String)o).getBytes(charset), index, impl);
	}
}
