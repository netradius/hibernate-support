/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Base type for encrypted user types. You may want to
 * 
 * @author Erik R. Jensen
 */
public abstract class EncryptedUserType implements UserType {

	/**
	 * The default SQL type for this user type.
	 *
	 * @see EncryptedUserType#sqlTypes()
	 */
	public static final int SQL_TYPE = Types.VARBINARY;

	/**
	 * The random number generator used to generate keys and initialization vectors.
	 *
	 * @see EncryptedUserType#generateKey(String, int)
	 * @see EncryptedUserType#generateIv(String)
	 */
	protected static SecureRandom rand = new SecureRandom();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] sqlTypes() {
		return new int[] {SQL_TYPE};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object nullSafeGet(
	    final ResultSet rs, final String[] names, final SharedSessionContractImplementor impl, final Object owner)
			throws HibernateException, SQLException {
		try {
			final byte[] data = rs.getBytes(names[0]);
			return rs.wasNull()
					? null
					: doFinal(getCipher(Cipher.DECRYPT_MODE), data);
		} catch (GeneralSecurityException x) {
			throw new HibernateException("Failed to retrieve encrypted value: " + x.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void nullSafeSet(
	    final PreparedStatement ps, final Object o, final int index, final SharedSessionContractImplementor impl)
			throws HibernateException, SQLException {
		try {
			if (o == null) {
				ps.setNull(index, SQL_TYPE);
			} else {
				ps.setBytes(index, doFinal(getCipher(Cipher.ENCRYPT_MODE), (byte[])o));
			}
		} catch (GeneralSecurityException x) {
			throw new HibernateException("Failed to set encrypted value: " + x.getMessage(), x);
		}
	}

	/**
	 * Gets the cipher to be used by this user type.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @return the cipher
	 * @throws GeneralSecurityException if an error occurs obtaining the cipher
	 */
	protected abstract Cipher getCipher(int mode) throws GeneralSecurityException;

	/**
	 * Preforms the encryption or decryption of the data.
	 *
	 * @param cipher the cipher to use
	 * @param data the data to encrypt or decrypt
	 * @return the encrypted or decrypted data
	 */
	protected byte[] doFinal(final Cipher cipher, final byte[] data) {
		try {
			return cipher.doFinal(data);
		} catch (IllegalBlockSizeException x) {
			throw new HibernateException("Invalid block size for transformation: " + x.getMessage(), x);
		} catch (BadPaddingException x) {
			throw new HibernateException("Data is not padded properly for transformation: " + x.getMessage(), x);
		}
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param provider the name of the provider (eg. JCE)
	 * @param key the encryption key
	 * @param iv the initialization vector or null
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String transformation, final Provider provider,
			final Key key, final IvParameterSpec iv) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		final Cipher cipher = provider == null
				? Cipher.getInstance(transformation)
				: Cipher.getInstance(transformation, provider);
		if (iv == null) {
			cipher.init(mode, key);
		} else {
			cipher.init(mode, key, iv);
		}
		return cipher;
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param provider the name of the provider (eg. JCE)
	 * @param key the encryption key
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String transformation, final Provider provider,
			final Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException {
		return getCipher(mode, transformation, provider, key, null);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param key the encryption key
	 * @param iv the initialization vector or null
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String transformation, final Key key,
		final IvParameterSpec iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException {
		return getCipher(mode, transformation, null, key, iv);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param key the encryption key
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String transformation, final Key key)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException {
		return getCipher(mode, transformation, null, key, null);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param algorithm the cryptographic algorithm (eg. AES)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param provider the name of the provider (eg. JCE)
	 * @param key the encryption key
	 * @param iv the initialization vector or null
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String algorithm, final String transformation,
			final String provider, final byte[] key, final byte[] iv) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		final Provider p = Security.getProvider(provider);
		if (p == null) {
			throw new HibernateException("Failed to find provider " + provider);
		}
		IvParameterSpec i = new IvParameterSpec(iv);
		Key k = new SecretKeySpec(key, algorithm);
		return getCipher(mode, transformation, p, k, i);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param algorithm the cryptographic algorithm (eg. AES)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param provider the name of the provider (eg. JCE)
	 * @param key the encryption key
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String algorithm, final String transformation,
			final String provider, final byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		return getCipher(mode, algorithm, transformation, provider, key, null);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param algorithm the cryptographic algorithm (eg. AES)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param key the encryption key
	 * @param iv the initialization vector or null
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String algorithm, final String transformation,
			final byte[] key, final byte[] iv) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		return getCipher(mode, algorithm, transformation, null, key, iv);
	}

	/**
	 * Helper method to create a cipher.
	 *
	 * @param mode the mode for the cipher (eg. Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE)
	 * @param algorithm the cryptographic algorithm (eg. AES)
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @param key the encryption key
	 * @return the created cipher
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 * @throws InvalidKeyException if the key is invalid
	 * @throws InvalidAlgorithmParameterException if the initialization vector is invalid
	 */
	public static Cipher getCipher(final int mode, final String algorithm, final String transformation,
			final byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException {
		return getCipher(mode, algorithm, transformation, null, key, null);
	}

	/**
	 * Helper method to generate a key.
	 *
	 * @param algorithm the cryptographic algorithm (eg. AES)
	 * @param size the size of the key to generate (eg. 128)
	 * @return the generated key
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 */
	public static Key generateKey(final String algorithm, final int size) throws NoSuchAlgorithmException {
		final KeyGenerator keygen = KeyGenerator.getInstance(algorithm);
		keygen.init(size);
		return keygen.generateKey();
	}

	/**
	 * Helper method to generate an initialization vector.
	 *
	 * @param transformation the name of the transformation (eg. AES/CBC/PKCS5Padding)
	 * @return the generated initialization vector
	 * @throws NoSuchAlgorithmException if the cryptographic algorithm cannot be found
	 * @throws NoSuchPaddingException the the padding algorithm cannot be found
	 */
	public static IvParameterSpec generateIv(final String transformation)
			throws NoSuchAlgorithmException, NoSuchPaddingException {
		final byte[] b = new byte[Cipher.getInstance(transformation).getBlockSize()];
		rand.nextBytes(b);
		return new IvParameterSpec(b);
	}
}
