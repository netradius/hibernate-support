/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User type to map Boolean values to fields of another type.
 *
 * @author Erik R. Jensen
 */
public abstract class BooleanUserType implements UserType {

	/**
	 * The SQL type.
	 */
	protected int sqlType;

	/**
	 * The object to represent true.
	 */
	protected Object trueValue;

	/**
	 * The object to represent false.
	 */
	protected Object falseValue;

	/**
	 * Creates a new BooleanUserType. The trueValue and falseValue need to be a valid type
	 * that can be returned from a JDBC ResultSet object. For example values of type
	 * Character will not work since database character types are returned as Strings.
	 *
	 * @param trueValue the value to represent true
	 * @param falseValue the value to represent false
	 * @param sqlType the SQL type
	 */
	public BooleanUserType(final Object trueValue, final Object falseValue, final int sqlType) {
		this.trueValue = trueValue;
		this.falseValue = falseValue;
		this.sqlType = sqlType;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object assemble(final Serializable cached, final Object owner)
			throws HibernateException {
		return cached;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object deepCopy(final Object obj) throws HibernateException {
		return obj;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Serializable disassemble(final Object obj) throws HibernateException {
		return (Serializable)obj;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public boolean equals(final Object obj1, final Object obj2) throws HibernateException {
		return obj1 == obj2 || (obj1 != null && obj2 != null && obj1.equals(obj2));
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public int hashCode(final Object obj) throws HibernateException {
		return obj.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object nullSafeGet(
	    final ResultSet rs, final String[] names, final SharedSessionContractImplementor impl, final Object owner)
      throws HibernateException, SQLException {
		final Object value = rs.getObject(names[0]);
		if (!rs.wasNull())
			return value.equals(trueValue);
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void nullSafeSet(
	    final PreparedStatement ps, final Object obj, int index, final SharedSessionContractImplementor impl)
      throws HibernateException, SQLException {
		if (obj == null)
			ps.setNull(index, sqlType);
		else if (obj.equals(true))
			ps.setObject(index, trueValue, sqlType);
		else
			ps.setObject(index, falseValue, sqlType);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object replace(final Object original, final Object target, final Object owner)
			throws HibernateException {
		return original;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Class<Boolean> returnedClass() {
		return Boolean.class;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public int[] sqlTypes() {
		return new int[] {sqlType};
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public boolean isMutable() {
		return false;
	}
}
