/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Extended version of MessageDigestUserType that uses a random salt in the message digest calculation.
 * This user type requires two columns to be defined. This first column will is where the message digest
 * is stored and the second column is where the randomly generated salt is stored. The size of the salt
 * generated can be overridden with the "saltSize" parameter. The salt is added to the end of the value
 * like "value{salt}" before it is digested.  In order to properly handle updates to entities using this
 * user type, you MUST have the "dynamic-update" property set to true on any entities that use this user
 * type.
 *
 * Do to a limitation in hibernate, if you need immediate access to the calculated message digest after an entity
 * has been saved or updated, you must re-query the value from your database.
 *
 * @author Erik R. Jensen
 */
public class SaltedMessageDigestUserType extends MessageDigestUserType {

	/**
	 * Convenience constant which can be used as the value for the type property of the @Type annotation.
	 */
	public static final String TYPE = "com.netradius.hibernate.support.SaltedMessageDigestUserType";

	/**
	 * The parameter name, "saltSize", used to override the default salt size.
	 */
	public static final String PARAM_SALT_SIZE = "saltSize";

	/**
	 * The default salt size which is 16 bytes.
	 */
	public static final int DEFAULT_SALT_SIZE = 16;

	/**
	 * The random number generated used to generate the salt.
	 */
	protected SecureRandom rand = new SecureRandom();

	/**
	 * The size of the salt to generate.
	 */
	protected int saltSize;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setParameterValues(Properties properties) {
		if (properties != null) {
			super.setParameterValues(properties);
			String tmp = properties.getProperty(PARAM_SALT_SIZE);
			if (tmp != null) {
				try {
					saltSize = Integer.parseInt(tmp);
				} catch (NumberFormatException x) {
					throw new HibernateException("Invalid salt size " + saltSize + ". Salt size must be an integer.");
				}
			} else {
				saltSize = DEFAULT_SALT_SIZE;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] sqlTypes() {
		return new int[] {SQL_TYPE, SQL_TYPE};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void nullSafeSet(PreparedStatement ps, Object value, int index, SharedSessionContractImplementor impl)
			throws HibernateException, SQLException {
		if (value == null) {
			super.nullSafeSet(ps, value, index, impl);
			ps.setNull(index + 1, SQL_TYPE);
		} else {
			final String salt = generateSalt();
			ps.setString(index, encode(digest(((String)value).getBytes(charset))));
			super.nullSafeSet(ps, saltPassword(value.toString(), salt), index, impl);
			ps.setString(index + 1, salt);
		}
	}

	/**
	 * Generates the salt value.
	 *
	 * @return the generated salt
	 */
	protected String generateSalt() {
		byte[] bytes = new byte[saltSize];
		rand.nextBytes(bytes);
		return encodeSalt(bytes);
	}

	/**
	 * Method to encode a byte[] array containing a salt.
	 *
	 * @param salt the salt to be encoded
	 * @return the encoded salt
	 */
	protected String encodeSalt(byte[] salt) {
		return encode(salt);
	}

	/**
	 * Returns the salted password. This method is useful if you wish to override the default salting
	 * scheme which is "password{salt}".
	 *
	 * @param password the password to salt
	 * @param salt the salt
	 * @return the salted password
	 */
	protected String saltPassword(String password, String salt) {
		return password + "{" + salt + "}";
	}
}
