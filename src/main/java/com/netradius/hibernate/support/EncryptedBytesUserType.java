/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import com.netradius.commons.lang.ArrayHelper;
import org.hibernate.HibernateException;

import java.io.Serializable;
import java.util.zip.Adler32;

/**
 * This user type handles encrypting and decrypting arrays of bytes.
 *
 * @author Erik R. Jensen
 */
public abstract class EncryptedBytesUserType extends EncryptedUserType {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class returnedClass() {
		return byte[].class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object o, final Object o1) throws HibernateException {
		return o == o1 || (o != null && o1 != null && ArrayHelper.equals((byte[])o, (byte[])o1));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode(final Object o) throws HibernateException {
		final Adler32 checksum = new Adler32();
		checksum.update((byte[])o);
		long val = checksum.getValue();
		return  ((int)val) ^ (int)(val >> 8);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object deepCopy(final Object value) throws HibernateException {
		byte[] b = (byte[])value;
		byte[] nb = new byte[b.length];
		System.arraycopy(b, 0, nb, 0, b.length);
		return nb;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMutable() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Serializable disassemble(final Object value) throws HibernateException {
		return (Serializable)deepCopy(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
		return deepCopy(cached);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
		return deepCopy(original);
	}
}
