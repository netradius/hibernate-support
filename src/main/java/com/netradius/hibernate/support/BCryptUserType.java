/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import org.mindrot.jbcrypt.BCrypt;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * A custom user type to leverage the blowfish password hashing scheme. In order to properly handle
 * updates to entities using this user type, you MUST have the "dynamic-update" property set to true
 * on any entities that use this user type.
 *
 * Do to a limitation in hibernate, if you need immediate access to the calculated message digest after
 * an entity has been saved or updated, you must re-query the value from your database.
 *
 * @author Erik R. Jensen
 */
public class BCryptUserType implements UserType, ParameterizedType {

	/**
	 * Convenience constant which can be used as the value for the type property of the @Type annotation.
	 */
	public static final String TYPE = "com.netradius.hibernate.support.BCryptUserType";

	/**
	 * The parameter name, "charset", used to override the default character set which is UTF8.
	 */
	public static final String PARAM_CHARSET = "charset";

	/**
	 * The default character set, "UTF8", which is used to convert the string value to bytes.
	 */
	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF8");

	/**
	 * The SQL type this user type maps to.
	 */
	public static final int SQL_TYPE = Types.VARCHAR;

	/**
	 * The pattern used to detect if the password has already been bcrypted.
	 */
	public static final Pattern BCRYPTED_PASSWORD_PATTERN = Pattern.compile("^\\$2a\\$\\d{2}\\$.*");

	/**
	 * The character set to use.
	 */
	protected Charset charset;

	/**
	 * {@inheritDoc}
	 */
  @Override
	public void setParameterValues(final Properties properties) {
		if (properties != null) {
			String tmp = properties.getProperty(PARAM_CHARSET);
			if (tmp != null) {
				try {
					charset = Charset.forName(tmp);
				} catch (IllegalCharsetNameException x) {
					throw new HibernateException("Unsupported character set " + tmp + ": " + x.getMessage(), x);
				} catch (UnsupportedCharsetException x) {
					throw new HibernateException("Unsupported character set " + tmp + ": " + x.getMessage(), x);
				}
			}
		}
		if (charset == null) {
			charset = DEFAULT_CHARSET;
		}
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public int[] sqlTypes() {
		return new int[] {SQL_TYPE};
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Class returnedClass() {
		return String.class;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public boolean equals(final Object o, final Object o1) throws HibernateException {
		return o == o1 || (o != null && o.equals(o1));
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public int hashCode(final Object o) throws HibernateException {
		return o.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object nullSafeGet(
	    final ResultSet rs, final String[] names, final SharedSessionContractImplementor impl, final Object owner)
			throws HibernateException, SQLException {
		final String val = rs.getString(names[0]);
		return rs.wasNull() ? null : val;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public void nullSafeSet(
	    final PreparedStatement ps, final Object value, final int index, final SharedSessionContractImplementor impl)
			throws HibernateException, SQLException {
		if (value == null) {
			ps.setNull(index, SQL_TYPE);
		} else if (BCRYPTED_PASSWORD_PATTERN.matcher(value.toString()).matches()) {
			ps.setString(index, value.toString());
		} else {
			ps.setString(index, BCrypt.hashpw(value.toString(), BCrypt.gensalt()));
		}
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object deepCopy(final Object o) throws HibernateException {
		return o;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public boolean isMutable() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Serializable disassemble(final Object o) throws HibernateException {
		return (Serializable)o;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object assemble(final Serializable serializable, final Object o) throws HibernateException {
		return serializable;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
		return original;
	}
}
