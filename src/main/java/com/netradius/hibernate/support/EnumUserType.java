/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * User type to map Enums to fields.
 *
 * @author Erik R. Jensen
 * 
 * @param <T> the Java type representing the value of the Enum
 * @param <E> the Enum type
 */
public abstract class EnumUserType<T, E extends Enum<E>> implements UserType {

	/**
	 * This SQL type for this user type.
	 *
	 * @see EncryptedUserType#sqlTypes()
	 */
	protected int sqlType;

	/**
	 * The class mapped by this user type.
	 */
	protected Class<E> clazz = null;

	/**
	 * Name to enum value mapping.
	 */
	protected HashMap<String, E> enumMap;

	/**
	 * Enum value to name mapping.
	 */
	protected HashMap<E, String> valueMap;

	/**
	 * Subclasses are required to call this constructor.
	 *
	 * @param clazz the Enum class
	 * @param enumValues the Enum values
	 * @param method the empty argument method which returns the value of the Enum
	 * @param sqlType the SQL type
	 * @throws NoSuchMethodException if the given method is not found
	 * @throws InvocationTargetException if there is an error calling the given method
	 * @throws IllegalAccessException if there is an error access the given method
	 */
	@SuppressWarnings("unchecked")
	public EnumUserType(final Class<E> clazz, final E[] enumValues, final String method, final int sqlType)
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		this.clazz = clazz;
		enumMap = new HashMap<String, E>(enumValues.length);
		valueMap = new HashMap<E, String>(enumValues.length);
		Method m = clazz.getMethod(method);
		for (E e: enumValues) {
			T value = (T)m.invoke(e);
			enumMap.put(value.toString(), e);
			valueMap.put(e, value.toString());
		}
		this.sqlType = sqlType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
		return cached;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object deepCopy(final Object obj) throws HibernateException {
		return obj;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Serializable disassemble(final Object obj) throws HibernateException {
		return (Serializable)obj;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object obj1, final Object obj2) throws HibernateException {
		return obj1 == obj2 || (obj1 != null && obj2 != null && obj1.equals(obj2));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode(final Object obj) throws HibernateException {
		return obj.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMutable() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object nullSafeGet(
	    final ResultSet rs, final String[] names, final SharedSessionContractImplementor impl, final Object owner)
			throws HibernateException, SQLException {
		final String value = rs.getString(names[0]);
		if (!rs.wasNull()) {
			return enumMap.get(value);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void nullSafeSet(
	    final PreparedStatement ps, final Object obj, final int index, final SharedSessionContractImplementor impl)
			throws HibernateException, SQLException {
		if (obj == null) {
			ps.setNull(index, sqlType);
		} else {
			ps.setObject(index, valueMap.get(obj), sqlType);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
		return original;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<E> returnedClass() {
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] sqlTypes() {
		return new int[] {sqlType};
	}
}
