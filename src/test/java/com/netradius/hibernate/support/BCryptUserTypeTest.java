/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Unit tests for BCryptUserType.
 *
 * @author Erik R. Jensen
 */
@Slf4j
public class BCryptUserTypeTest {

	private static final String PASSWORD = "enterprise";
	private static HibernateHelper helper;

	@BeforeClass
	public static void before() {
		helper = new HSQLHibernateHelper()
				.init(Example.class);
	}

	@AfterClass
	public static void after() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new BCryptUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example example = new Example(PASSWORD);
		ses.save(example);
		tx.commit();
		Record rec = getRecord(example.getId());
		log.info(rec.toString());
		assertTrue(BCrypt.checkpw(PASSWORD, rec.getPassword()));
		assertFalse(BCrypt.checkpw("Picard", rec.getPassword()));

		ses = helper.getSession();
		tx = ses.beginTransaction();
		example = ses.get(Example.class, example.getId());
		log.info(example.toString());
		example.setPassword("Warf");
		ses.save(example);
		tx.commit();
		rec = getRecord(example.getId());
		log.info(rec.toString());
		assertTrue(BCrypt.checkpw("Warf", rec.getPassword()));
		assertFalse(BCrypt.checkpw("Picard", rec.getPassword()));

		ses = helper.getSession();
		tx = ses.beginTransaction();
		example = ses.get(Example.class, example.getId());
		log.info(example.toString());
		example.setText("Data");
		ses.update(example);
		tx.commit();
		rec = getRecord(example.getId());
		log.info(rec.toString());
		assertTrue(BCrypt.checkpw("Warf", rec.getPassword()));
		assertFalse(BCrypt.checkpw("Picard", rec.getPassword()));
	}

	private Record getRecord(Long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<Record>(Record.class), id);
	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example implements Serializable {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "text")
		private String text;

		@Column(name = "password", length = 60)
		@Type(type = BCryptUserType.TYPE)
		private String password;

		public Example(String password) {
			this.password = password;
		}
	}

	@Data
	public static class Record {
		private Long id;
		private String password;
	}
}
