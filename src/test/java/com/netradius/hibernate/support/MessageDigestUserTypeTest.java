/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.*;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for MessageDigestUserType.
 * 
 * @author Erik R. Jensen
 */
@Slf4j
public class MessageDigestUserTypeTest {

	private static final String TXT = "One damn minute, Admiral.";
	private static final String MD5 = "8A399C0878CEEEA1A63E53AEC2CAF896";
	private static final String SHA1 = "2F6A4E591194BF02EA503DB35D06685AB0CEC109";

	private static HibernateHelper helper;

	@BeforeClass
	public static void before() {
		helper = new HSQLHibernateHelper()
				.init(Example.class);
	}

	@AfterClass
	public static void after() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new MessageDigestUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example data = new Example(TXT);
		ses.save(data);
		tx.commit();

		Record rec = getRecord(data.getId());
		log.info(rec.toString());
		assertEquals(MD5, rec.getMd5());
		assertEquals(SHA1, rec.getSha1());

		ses = helper.getSession();
		tx = ses.beginTransaction();
		data = ses.get(Example.class, data.getId());
		log.info(data.toString());
		data.setText("test");
		ses.save(data);
		tx.commit();
		rec = getRecord(data.getId());
		log.info(rec.toString());
		assertEquals(MD5, rec.getMd5());
		assertEquals(SHA1, rec.getSha1());
	}

	private Record getRecord(Long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<>(Record.class), id);
	}

	@Entity
	@Table(name = "example")
	@DynamicUpdate // Prior to Hibernate 4.1.4 you had to use the deprecated @org.hibernate.annotations.Entity(dynamicUpdate = true)
	@Data
	@NoArgsConstructor
	public static class Example {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "text")
		private String text;

		@Column(name = "md5")
		@Type(type = MessageDigestUserType.TYPE,
				parameters = @Parameter(name = MessageDigestUserType.PARAM_ALGORITHM, value = "MD5"))
		private String md5;

		@Column(name = "sha1")
		@Type(type = MessageDigestUserType.TYPE,
				parameters = @Parameter(name = MessageDigestUserType.PARAM_ALGORITHM, value = "SHA1"))
		private String sha1;

		public Example(String text) {
			this.text = text;
			this.md5 = text;
			this.sha1 = text;
		}
	}

	@Data
	public static class Record {

		private Long id;
		private String text;
		private String md5;
		private String sha1;

	}
}
