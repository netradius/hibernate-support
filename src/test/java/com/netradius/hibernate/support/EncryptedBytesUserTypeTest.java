/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Unit tests for EncryptedBytesUserType.
 *
 * @author Erik R. Jensen
 */
@Slf4j
public class EncryptedBytesUserTypeTest {

	private static final Charset UTF8 = Charset.forName("UTF8");
	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	private static final int KEY_SIZE = 128;
	private static final String MESSAGE = "You want chaos? I'm the chaos. You want fear? I'm the fear. You want a new beginning? I'm the new beginning!";

	private static HibernateHelper helper;

	@BeforeClass
	public static void before() {
		helper = new HSQLHibernateHelper()
				.init(Example.class);
	}

	@AfterClass
	public static void after() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new AESEncryptedBytesUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example data = new Example(MESSAGE.getBytes(UTF8));
		ses.save(data);
		tx.commit();
		Record rec = getRecord(data.getId());
		log.info(rec.toString());

		ses = helper.getSession();
		tx = ses.beginTransaction();
		data = (Example)ses.get(Example.class, data.getId());
		tx.commit();
		log.info(data.toString());
		assertEquals(new String(data.getMessage(), UTF8), MESSAGE);
	}

	public Record getRecord(long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<Record>(Record.class), id);
	}

	public static class AESEncryptedBytesUserType extends EncryptedBytesUserType {

		private Key key = null;
		private IvParameterSpec iv = null;

		@Override
		protected Cipher getCipher(int mode) throws GeneralSecurityException {
			if (key == null) {
				key = EncryptedStringUserType.generateKey(ALGORITHM, KEY_SIZE);
				iv = generateIv(TRANSFORMATION);
			}
			return getCipher(mode, TRANSFORMATION, key, iv);
		}
	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example implements Serializable {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "message", length = 1000)
		@Type(type = "com.netradius.hibernate.support.EncryptedBytesUserTypeTest$AESEncryptedBytesUserType")
		private byte[] message;

		public Example(byte[] message) {
			this.message = message;
		}
	}

	@Data
	public static class Record {
		private Long id;
		private byte[] message;
	}
}
