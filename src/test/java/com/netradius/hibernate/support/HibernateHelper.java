/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;

/**
 * @author Erik R. Jensen
 */
@Slf4j
@Data
@Accessors(chain = true)
public abstract class HibernateHelper {

	@Setter(AccessLevel.NONE)
	private SessionFactory sessionFactory;

	public abstract String getDialect();

	public abstract String getDriver();

	public abstract String getUrl();

	public abstract String getUsername();

	public abstract String getPassword();

	public HibernateHelper init(Class<?> ... classes) {
		log.debug("Initializing hibernate.");
		Configuration config = new Configuration()
				.setProperty("hibernate.dialect", getDialect())
				.setProperty("hibernate.connection.driver_class", getDriver())
				.setProperty("hibernate.connection.url", getUrl())
				.setProperty("hibernate.connection.username", getUsername())
				.setProperty("hibernate.connection.password", getPassword())
				.setProperty("hibernate.connection.pool_size", "1")
				.setProperty("hibernate.connection.autocommit", "false")
				.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider")
				.setProperty("hibernate.hbm2ddl.auto", "create-drop")
				.setProperty("hibernate.show_sql", "false")
				.setProperty("hibernate.current_session_context_class", "thread");
		for (Class<?> clazz: classes) {
			log.debug("Loading class " + clazz.getName());
			config.addAnnotatedClass(clazz);
		}
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties())
				.build();
		sessionFactory = config.buildSessionFactory(serviceRegistry);
		config.buildSessionFactory(serviceRegistry);
		log.debug("Hibernate initialized.");
		return this;
	}

	/**
	 * Closes the Hibernate session factory and related resources.
	 */
	public void close() {
		if (sessionFactory != null) {
			log.debug("Closing hibernate.");
			sessionFactory.close();
			sessionFactory = null;
			log.debug("Hibernate closed.");
		}
	}

	/**
	 * Returns the current Hibernate session.
	 *
	 * @return the hibernate session
	 */
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * Returns a JDBC connection from the Hibernate session factory.
	 *
	 * @return the JDBC connection
	 * @throws SQLException if an error occurred obtaining a connection
	 */
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(getUrl(), getUsername(), getPassword());
	}

	@SuppressWarnings("unchecked")
	public <T> T query(String query, ResultSetHandler<T> handler, Object... params) throws SQLException {
		final Connection con = getConnection();
		try {
			return (T) new QueryRunner().query(con, query, handler, params);
		} finally {
			DbUtils.close(con);
		}
	}

	/**
	 * Utility method to close JDBC connections, statements and result sets.
	 *
	 * @param con the connection to close or null
	 * @param stmt the statement to close or null
	 * @param rs the result set to close or null
	 */
	public void close(Connection con, Statement stmt, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException x) {
				log.error("Error closing result set: " + x.getMessage(), x);
			}
		}
		if (stmt != null) {
			try {
				con.close();
			} catch (SQLException x) {
				log.error("Error closing statement: " + x.getMessage(), x);
			}
		}
		if (con != null) {
			try {
				con.close();
			} catch (SQLException x) {
				log.error("Error closing connection: " + x.getMessage(), x);
			}
		}
	}

	/**
	 * Prints out all table details to stdout.
	 */
	public HibernateHelper printTables() {
		prettyPrint(getTables());
		return this;
	}

	/**
	 * Prints out the column details for a given table name to stdout.
	 *
	 * @param table the table name
	 */
	public HibernateHelper printColumns(String table) {
		prettyPrint(getColumns(table));
		return this;
	}

	private List<String[]> getTables() {
		final List<String[]> rows = new ArrayList<String[]>();
		rows.add(new String[] {
				"TABLE_CAT",
				"TABLE_SCHEM",
				"TABLE_NAME",
				"TABLE_TYPE",
				"REMARKS",
				"TYPE_CAT",
				"TYPE_SCHEM",
				"TYPE_NAME",
				"SELF_REFERENCING_COL_NAME",
				"REF_GENERATION "
		});
		Connection con = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			final DatabaseMetaData md = con.getMetaData();
			rs = md.getTables(null, null, "%", null);
			while (rs.next()) {
				List<String> s = new ArrayList<String>(10);
				for (int i = 10; i > 0; i--)
					s.add(rs.getString(i));
				rows.add(s.toArray(new String[10]));
			}
		} catch (SQLException x) {
			log.error("Error listing tables: " + x.getMessage(), x);
		} finally {
			close(con, null, rs);
		}
		return rows;
	}

	private List<String[]> getColumns(final String table) {
		final List<String[]> rows = new ArrayList<String[]>();
		rows.add(new String[] {
				"TABLE_CAT",
				"TABLE_SCHEM",
				"TABLE_NAME",
				"COLUMN_NAME",
				"DATA_TYPE",
				"TYPE_NAME",
				"COLUMN_SIZE",
				"BUFFER_LENGTH",
				"DECIMAL_DIGITS",
				"NUM_PREC_RADIX",
				"NULLABLE",
				"REMARKS",
				"COLUMN_DEF",
				"SQL_DATA_TYPE",
				"SQL_DATETIME_SUB",
				"CHAR_OCTET_LENGTH",
				"ORDINAL_POSITION",
				"IS_NULLABLE",
				"SCOPE_CATLOG",
				"SCOPE_SCHEMA",
				"SCOPE_TABLE",
				"SOURCE_DATA_TYPE",
				"IS_AUTOINCREMENT"
		});
		Connection con = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			final DatabaseMetaData md = con.getMetaData();
			rs = md.getColumns(null, null, table, "%");
			while (rs.next()) {
				List<String> s = new ArrayList<String>(23);
				for (int i = 23; i > 0; i--)
					s.add(rs.getString(i));
				rows.add(s.toArray(new String[23]));
			}
		} catch (SQLException x) {
			log.error("Error describing table: " + x.getMessage(), x);
		} finally {
			close(con, null, rs);
		}
		return rows;
	}

	/**
	 * Pretty prints a list of String arrays. This method assumes the first array in the list
	 * is the header.
	 *
	 * @param rows the rows to print
	 */
	private void prettyPrint(List<String[]> rows) {
		if (!rows.isEmpty()) {
			final int numCol = rows.get(0).length;
			final int[] maxLength = new int[numCol];
			Arrays.fill(maxLength, 0);
			for (String[] row: rows) {
				for (int i = row.length - 1; i >= 0; i--) {
					if (row[i] == null && maxLength[i] < 4)
						maxLength[i] = 4;
					else if (row[i] != null && row[i].length() > maxLength[i])
						maxLength[i] = row[i].length();
				}
			}
			final StringBuilder sb = new StringBuilder();
			int totalLength = 0;
			for (int i = 0; i < maxLength.length; i++) {
				totalLength += maxLength[i];
				if (i == 0)
					sb.append("| ");
				sb.append("%").append(i + 1).append("$-").append(maxLength[i]).append("s | ");
				if (i == maxLength.length - 1)
					sb.append("\n");
			}
			totalLength += numCol * 3 + 1;
			final String pattern = sb.toString();
			final Formatter formatter = new Formatter(System.out);
			System.out.print(line('=', totalLength));
			for (int i = 0; i < rows.size(); i++) {
				formatter.format(pattern, (Object[])rows.get(i));
				if (i == 0)
					System.out.print(line('=',totalLength));
				else
					System.out.print(line('-',totalLength));
			}
		}
	}

	private String line(char c, int num) {
		final StringBuilder sb = new StringBuilder();
		for (; num > 0; num--) {
			sb.append(c);
		}
		return sb.append("\n").toString();
	}

}
