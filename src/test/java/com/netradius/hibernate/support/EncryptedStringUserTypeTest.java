/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.persistence.*;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for EncryptedStringUserType.
 * 
 * @author Erik R. Jensen
 */
@Slf4j
public class EncryptedStringUserTypeTest {

	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	private static final int KEY_SIZE = 128;
	private static final String MESSAGE = "Why oh why didn't I take the BLUE pill?";

	private static HibernateHelper helper;

	@BeforeClass
	public static void before() {
		helper = new HSQLHibernateHelper()
				.init(Example.class);
	}

	@AfterClass
	public static void after() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new AESEncryptedStringUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example data = new Example(MESSAGE);
		ses.save(data);
		tx.commit();
		Record rec = getRecord(data.getId());
		log.info(rec.toString());

		ses = helper.getSession();
		tx = ses.beginTransaction();
		data = ses.get(Example.class, data.getId());
		tx.commit();
		log.info(data.toString());
		assertEquals(data.getMessage(), MESSAGE);
	}

	public Record getRecord(Long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<>(Record.class), id);
	}

	public static class AESEncryptedStringUserType extends EncryptedStringUserType {

		private Key key = null;
		private IvParameterSpec iv = null;
		
		@Override
		protected Cipher getCipher(int mode) throws GeneralSecurityException {
			if (key == null) {
				key = EncryptedStringUserType.generateKey(ALGORITHM, KEY_SIZE);
				iv = generateIv(TRANSFORMATION);
			}
			return getCipher(mode, TRANSFORMATION, key, iv);
		}
	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example implements Serializable {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "message")
		@Type(type = "com.netradius.hibernate.support.EncryptedStringUserTypeTest$AESEncryptedStringUserType")
		private String message;

		public Example(String message) {
			this.message = message;
		}
	}

	@Data
	public static class Record {
		private Long id;
		private byte[] message;
	}
}
