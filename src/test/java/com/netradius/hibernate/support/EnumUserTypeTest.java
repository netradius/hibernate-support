/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.hibernate.criterion.Restrictions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test for EnumUserType.
 *
 * @author Erik R. Jensen
 */
@Slf4j
public class EnumUserTypeTest {

	private static HibernateHelper helper;

	@BeforeClass
	public static void classSetup() {
		helper = new HSQLHibernateHelper()
				.init(Example.class)
				.printTables()
				.printColumns("EXAMPLE");
	}

	@AfterClass
	public static void classTearDown() {
		helper.close();
	}

	@Test
	public void testNullEquals() throws Exception {
		assertTrue(new ComplexStatusUserType().equals(null, null));
	}

	@Test
	public void test() {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example dataA = new Example(Status.ACTIVE, ComplexStatus.ACTIVE, NumericStatus.ACTIVE);
		ses.save(dataA);
		Example dataI = new Example(Status.INACTIVE, ComplexStatus.INACTIVE, NumericStatus.INACTIVE);
		ses.save(dataI);
		Example data = new Example();
		ses.save(data);
		tx.commit();

		ses = helper.getSession();
		tx = ses.beginTransaction();
		Record record = getRecord(dataA.getId());
		assertNotNull(record);
		assertEquals(record.getStatus(), Status.ACTIVE.toString());
		assertEquals(record.getComplexStatus(), ComplexStatus.ACTIVE.getValue());
		assertTrue(record.getNumericStatus() == NumericStatus.ACTIVE.getValue());
		record = getRecord(dataI.getId());
		assertNotNull(record);
		assertEquals(record.getStatus(), Status.INACTIVE.toString());
		assertEquals(record.getComplexStatus(), ComplexStatus.INACTIVE.getValue());
		assertTrue(record.getNumericStatus() == NumericStatus.INACTIVE.getValue());
		record = getRecord(data.getId());
		assertNotNull(record);
		assertNull(record.getStatus());
		assertNull(record.getComplexStatus());
		assertNull(record.getNumericStatus());
		tx.commit();

		ses = helper.getSession();
		tx = ses.beginTransaction();
		dataA = findById(ses, dataA.getId());
		assertNotNull(dataA);
		assertEquals(dataA.getStatus(), Status.ACTIVE);
		assertEquals(dataA.getComplexStatus(), ComplexStatus.ACTIVE);
		assertEquals(dataA.getNumericStatus(), NumericStatus.ACTIVE);
		dataI = findById(ses, dataI.getId());
		assertNotNull(dataI);
		assertEquals(dataI.getStatus(), Status.INACTIVE);
		assertEquals(dataI.getComplexStatus(), ComplexStatus.INACTIVE);
		assertEquals(dataI.getNumericStatus(), NumericStatus.INACTIVE);
		data = findById(ses, data.getId());
		assertNotNull(data);
		assertNull(data.getStatus());
		assertNull(data.getComplexStatus());
		assertNull(data.getNumericStatus());
		tx.commit();
	}

	@SuppressWarnings("unchecked")
	private Example findById(Session ses, Long id) {
    CriteriaBuilder builder = ses.getCriteriaBuilder();
    CriteriaQuery<Example> query = builder.createQuery(Example.class);
    Root<Example> root = query.from(Example.class);
    query.select(root).where(builder.equal(root.get("id"), id));
		final List<Example> lst = ses.createQuery(query).getResultList();
		return lst.isEmpty() ? null : lst.get(0);
	}

	private Record getRecord(Long id) {
		final String query = "select status, complex_status, numeric_status from example where data_id = ?";
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = helper.getConnection();
			ps = con.prepareStatement(query);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				Record record = new Record();
				record.setStatus(rs.getString(1));
				record.setComplexStatus(rs.getString(2));
				record.setNumericStatus(rs.getInt(3));
				if (rs.wasNull())
					record.setNumericStatus(null);
				return record;
			}
		} catch (SQLException x) {
			log.error("Error executing query: " + x.getMessage(), x);
		} finally {
			helper.close(con, ps, rs);
		}
		return null;
	}

	@Data
	public static class Record {

		private String status;
		private String complexStatus;
		private Integer numericStatus;

	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example {

		@Id
		@GeneratedValue(strategy= GenerationType.AUTO)
		@Column(name="data_id")
		private Long id;

		@Column(name="status", length=8)
		@Type(type="com.netradius.hibernate.support.EnumUserTypeTest$StatusUserType")
		private Status status;

		@Column(name="complex_status", columnDefinition = "char(1)")
		@Type(type="com.netradius.hibernate.support.EnumUserTypeTest$ComplexStatusUserType")
		private ComplexStatus complexStatus;

		@Column(name="numeric_status")
		@Type(type="com.netradius.hibernate.support.EnumUserTypeTest$NumericStatusUserType")
		private NumericStatus numericStatus;

		public Example(Status status, ComplexStatus complexStatus, NumericStatus numericStatus) {
			this.status = status;
			this.complexStatus = complexStatus;
			this.numericStatus = numericStatus;
		}
	}

	public static enum Status {
		ACTIVE,
		INACTIVE
	}

	public static class StatusUserType extends EnumUserType<String, Status> {

		public StatusUserType() throws NoSuchMethodException,
				InvocationTargetException, IllegalAccessException {
			super(Status.class, Status.values(), "toString", Types.VARCHAR);
		}
		
	}

	public static enum ComplexStatus {

		ACTIVE("A"),
		INACTIVE("I");

		private String value;

		private ComplexStatus(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public static class ComplexStatusUserType extends EnumUserType<String, ComplexStatus> {

		public ComplexStatusUserType() throws NoSuchMethodException,
				InvocationTargetException, IllegalAccessException {
			super(ComplexStatus.class, ComplexStatus.values(), "getValue", Types.CHAR);
		}

	}

	public static enum NumericStatus {

		ACTIVE(1),
		INACTIVE(2);

		private int value;

		private NumericStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public static class NumericStatusUserType extends EnumUserType<Integer, NumericStatus> {

		public NumericStatusUserType() throws NoSuchMethodException,
				InvocationTargetException, IllegalAccessException {
			super(NumericStatus.class, NumericStatus.values(), "getValue", Types.INTEGER);
		}
		
	}
}
