/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import java.security.Provider;
import java.security.Security;

/**
 * Provides JCA/JCE related utility methods.
 *
 * @author Erik R. Jensen
 */
public final class JCEUtil {

	private JCEUtil() {}

	public static void printProviderInformation(String providerName) {
		Provider provider = Security.getProvider(providerName);
		StringBuilder sb = new StringBuilder("\n")
				.append("Name:       ").append(provider.getName()).append("\n")
				.append("Version:    ").append(provider.getVersion()).append("\n")
				.append("Info:       ").append(provider.getInfo()).append("\n")
				.append("Services:\n");
		for (Provider.Service service: provider.getServices()) {
			sb.append("  ").append(service.getAlgorithm()).append("\n")
					.append("    Type: ").append(service.getType()).append("\n")
					.append("    Class Name: ").append(service.getClassName()).append("\n");
		}
		System.out.print(sb.toString());
	}

	public static void printProviderInformation() {
		System.out.println("Providers:");
		for (Provider p: Security.getProviders()) {
			printProviderInformation(p.getName());
		}
		System.out.println();
	}
}
