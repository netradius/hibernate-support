/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.*;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for PgJsonBStringUserType.
 *
 * @author Erik R. Jensen
 */
@Slf4j
@Ignore
public class PgJsonBStringUserTypeTest {

	private static HibernateHelper helper;

	@BeforeClass
	public static void before() {
		helper = new PGHibernateHelper()
				.init(Example.class);

		// drop example if exists
	}

	@AfterClass
	public static void after() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new PgJsonBStringUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException {
		String json = "{\"test\": \"example\"}";
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example data = new Example(json);
		ses.save(data);
		tx.commit();

		Record rec = getRecord(data.getId());
		log.info(rec.toString());
		assertEquals(json, rec.getJson());

	}

	private Record getRecord(Long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<>(Record.class), id);
	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "json")
		@Type(type = "com.netradius.hibernate.support.PgJsonBStringUserType")
		private String json;

		public Example(String json) {
			this.json = json;
		}
	}

	@Data
	public static class Record {

		private Long id;
		private String json;

	}
}
