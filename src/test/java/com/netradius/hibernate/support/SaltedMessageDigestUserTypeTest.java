/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for SalesMessageDigestUserType.
 * 
 * @author Erik R. Jensen
 */
@Slf4j
public class SaltedMessageDigestUserTypeTest {

	private static final String TXT = "Lock s-foils in attack position.";
	private static final Charset UTF8 = Charset.forName("UTF8");

	private static HibernateHelper helper;

	@BeforeClass
	public static void classSetup() {
		helper = new HSQLHibernateHelper()
				.init(Example.class);
	}

	@AfterClass
	public static void classTearDown() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new SaltedMessageDigestUserType().equals(null, null));
	}

	@Test
	public void test() throws SQLException, NoSuchAlgorithmException {
		MessageDigest mdMd5 = MessageDigest.getInstance("MD5");
		MessageDigest mdSha1 = MessageDigest.getInstance("SHA1");

		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example data = new Example(TXT);
		ses.save(data);
		tx.commit();
		Record rec = getRecord(data.getId());
		String md5 = BitTwiddler.tohexstr(mdMd5.digest((TXT + "{" + rec.getMd5salt() + "}").getBytes(UTF8)), false, true);
		String sha1 = BitTwiddler.tohexstr(mdSha1.digest((TXT + "{" + rec.getSha1salt() + "}").getBytes(UTF8)), false, true);
		log.info(rec.toString());
		assertEquals(rec.getMd5(), md5);
		assertEquals(rec.getSha1(), sha1);

		ses = helper.getSession();
		tx = ses.beginTransaction();
		data = ses.get(Example.class, data.getId());
		log.info(data.toString());
		data.setText("test");
		ses.save(data);
		tx.commit();
		rec = getRecord(data.getId());
		log.info(rec.toString());
		assertEquals(rec.getMd5(), md5);
		assertEquals(rec.getSha1(), sha1);
	}

	private Record getRecord(Long id) throws SQLException {
		return helper.query("SELECT * FROM example WHERE id = ?", new BeanHandler<Record>(Record.class), id);
	}

	@Entity
	@Table(name = "example")
	@DynamicUpdate // Prior to Hibernate 4.1.4 you had to use the deprecated @org.hibernate.annotations.Entity(dynamicUpdate = true)
	@Data
	@NoArgsConstructor
	public static class Example implements Serializable {

		public Example(String text) {
			this.text = text;
			this.md5 = text;
			this.sha1 = text;
		}

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Long id;

		@Column(name = "text")
		private String text;

		@Columns(columns = {
				@Column(name = "md5"),
				@Column(name = "md5salt")
		})
		@Type(type = SaltedMessageDigestUserType.TYPE,
				parameters = @Parameter(name = SaltedMessageDigestUserType.PARAM_ALGORITHM, value = "MD5"))
		private String md5;

		@Column(name = "md5salt", updatable = false, insertable = false)
		private String md5salt;

		@Columns(columns = {
				@Column(name = "sha1"),
				@Column(name = "sha1salt")
		})
		@Type(type = SaltedMessageDigestUserType.TYPE,
				parameters = @Parameter(name = SaltedMessageDigestUserType.PARAM_ALGORITHM, value = "SHA1"))
		private String sha1;

		@Column(name = "sha1Salt", updatable = false, insertable = false)
		private String sha1salt;
	}

	@Data
	public static class Record {

		private Long id;
		private String text;
		private String md5;
		private String md5salt;
		private String sha1;
		private String sha1salt;

	}
}
