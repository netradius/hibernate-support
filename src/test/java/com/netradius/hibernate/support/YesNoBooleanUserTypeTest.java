/**
 * Copyright (c) 2010-2019, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.hibernate.support;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test for YesNoBooleanUserType.
 *
 * @author Erik R. Jensen
 */
@Slf4j
public class YesNoBooleanUserTypeTest {

	private static HibernateHelper helper;

	@BeforeClass
	public static void classSetup() {
		helper = new HSQLHibernateHelper()
				.init(Example.class)
				.printTables()
				.printColumns("EXAMPLE");
	}

	@AfterClass
	public static void classTearDown() {
		helper.close();
	}

	@Test
	public void testNullEquals() {
		assertTrue(new YesNoBooleanUserType().equals(null, null));
	}

	@Test
	public void test() {
		Session ses = helper.getSession();
		Transaction tx = ses.beginTransaction();
		Example dataY = new Example(true);
		ses.save(dataY);
		Example dataN = new Example(false);
		ses.save(dataN);
		Example data = new Example();
		ses.save(data);
		tx.commit();

		assertEquals(getValue(dataY.getId()), "Y");
		assertEquals(getValue(dataN.getId()), "N");
		assertNull(getValue(data.getId()));

		ses = helper.getSession();
		tx = ses.beginTransaction();
		dataY = findById(ses, dataY.getId());
		assertNotNull(dataY);
		assertTrue(dataY.getValue());
		dataN = findById(ses, dataN.getId());
		assertNotNull(dataN);
		assertFalse(dataN.getValue());
		data = findById(ses, data.getId());
		assertNotNull(data);
		assertNull(data.getValue());
		tx.commit();
	}

	@SuppressWarnings("unchecked")
	private Example findById(Session ses, Long id) {
    CriteriaBuilder cb = ses.getCriteriaBuilder();
    CriteriaQuery cq = cb.createQuery(Example.class);
    Root<Example> root = cq.from(Example.class);
    cq.select(root).where(cb.equal(root.get("id"), id));
    List<Example> lst = ses.createQuery(cq).getResultList();
    return lst.isEmpty() ? null : lst.get(0);
	}

	private String getValue(Long id) {
		final String query = "select value from example where data_id = ?";
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = helper.getConnection();
			ps = con.prepareStatement(query);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getString(1);
		} catch (SQLException x) {
			log.error("Error executing query: " + x.getMessage(), x);
		} finally {
			helper.close(con, ps, rs);
		}
		return null;
	}

	@Entity
	@Table(name = "example")
	@Data
	@NoArgsConstructor
	public static class Example {

		@Id
		@GeneratedValue(strategy= GenerationType.AUTO)
		@Column(name="data_id")
		private Long id;

		@Column(name="value", columnDefinition = "char(1)")
		@Type(type="com.netradius.hibernate.support.YesNoBooleanUserType")
		private Boolean value;

		public Example(Boolean value) {
			this.value = value;
		}
	}
}
