# Hibernate Support Library

This library provides useful UserType classes and support classes that may be 
useful for projects using the Java ORM [Hibernate](http://www.hibernate.org/).

Each UserType has a unit test associated with it. Please see the unit test for
examples on how to use each UserType class.

## Useful Links
 * [Hibernate Releases & Compatibility Matrix](https://hibernate.org/orm/releases/)
 * [Hibernate in Maven Central](https://repo.maven.apache.org/maven2/org/hibernate/hibernate-core/)
 * [Hibernate Git Repository](https://github.com/hibernate/hibernate-orm)
